package pwdValidator;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author Jawad Khan | 991556484
 * 
 * Test class for the Validator Class
 *
 */

public class ValidatorTest {
	
	/**
	 * Unit Tests for checking password length
	 */

	@Test
	public void testIsValidLengthRegular() {
		boolean isValid = Validator.isValidLength("testlength");
		assertTrue("Password length is invalid", isValid);
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean isValid = Validator.isValidLength("");
		assertFalse("Password length is invalid", isValid);
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean isValid = Validator.isValidLength(" e i g h t");
		assertFalse("Password length is invalid", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValid = Validator.isValidLength("testtest");
		assertTrue("Password length is invalid", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValid = Validator.isValidLength("testtes ");
		assertFalse("Password length is invalid", isValid);
	}
	
	/**
	 * Unit Tests for checking if two digits are present
	 */
	
	@Test
	public void testHasValidDigitsRegular() {
		boolean isValid = Validator.hasValidDigits("6test2digits9");
		assertTrue("Password does not have minimum digits", isValid);
	}
	
	@Test
	public void testHasValidDigitsException() {
		 boolean isValid = Validator.hasValidDigits("testdigits");
		 assertFalse("Password does not have minimum digits", isValid);
	}
	
	@Test
	public void testHasValidDigitsExceptionBlank() {
		 boolean isValid = Validator.hasValidDigits("");
		 assertFalse("Password does not have minimum digits", isValid);
	}
	
	@Test
	public void testHasValidDigitsBoundaryIn() {
		boolean isValid = Validator.hasValidDigits("12");
		assertTrue("Password does not have minimum digits", isValid);
	}
	
	@Test
	public void testHasValidDigitsBoundaryOut() {
		boolean isValid = Validator.hasValidDigits("-1");
		assertFalse("Password does not have minimum digits", isValid);
	}
	
	@Test
	public void testHasUpperAndLowerCharsRegular() {
		boolean isValid = Validator.hasUpperAndLowerChars("AbAbAbAb");
		assertTrue("Password does not have both uppercase and lowercase characters", isValid);
	}
	
	@Test
	public void testHasUpperAndLowerCharsExceptionalJustLower() {
		boolean isValid = Validator.hasUpperAndLowerChars("aaaaaaaa");
		assertFalse("Password does not have both uppercase and lowercase characters", isValid);
	}
	
	@Test
	public void testHasUpperAndLowerCharsExceptionalJustUpper() {
		boolean isValid = Validator.hasUpperAndLowerChars("BBBBCCCC");
		assertFalse("Password does not have both uppercase and lowercase characters", isValid);
	}
	
	@Test
	public void testHasUpperAndLowerCharsBoundaryIn() {
		boolean isValid = Validator.hasUpperAndLowerChars("abcdefgH");
		assertTrue("Password does not have both uppercase and lowercase characters", isValid);
	}
	
	@Test
	public void testHasUpperAndLowerCharsBoundaryOut() {
		boolean isValid = Validator.hasUpperAndLowerChars("1234567a");
		assertFalse("Password does not have both uppercase and lowercase characters", isValid);
	}
	
}