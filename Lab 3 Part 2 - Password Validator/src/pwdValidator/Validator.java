package pwdValidator;

/**
 * 
 * @author Jawad Khan | 991556484
 * 
 * The class validates passwords to meet the minimum requirements
 * which are it must be at least 8 character with at least 2 digits
 * 
 * Created using TDD
 *
 */

public class Validator {
	
	private static int MIN_LENGTH = 8;

	/**
	 * 
	 * @param pwd
	 * @return true if length of password is >= 8.  Spaces are not counted as part of the length.
	 * 
	 * Refactor History:
	 * 	Initially checked string length using if statements and had multiple returns
	 * 	Was reduced to single return statement checking string length
	 *  Further refactored to check exceptional case where spaces are counted as characters
	 *  	This was done using an if statement and multiple returns
	 *  Multiple returns reduced to single statement that checks for spaces and length
	 *  Removed .trim() on the string as statement already checks for spaces
	 */
	public static boolean isValidLength (String pwd) {
		return pwd.indexOf(" ") < 0 && pwd.length() >= MIN_LENGTH;
	}
	
	/**
	 * 
	 * @param pwd
	 * @return true if the count of digits in the pwd is greater than two (2)
	 * 
	 * Refactor History:
	 * Method initially return true to validate the test case
	 * Loops through the string as a character array and checks each character to see
	 * if it's a digit.
	 */
	public static boolean hasValidDigits (String pwd) {
		int count = 0;
		
		for ( char c : pwd.toCharArray() ) {
			if ( Character.isDigit(c) ) {
				count++;
			}
		}
		return count >= 2;
	}
	
	/**
	 * @param pwd
	 * @return true only if the passed string has both uppercase AND lowercase characters
	 * 
	 * Refactor History:
	 * Method initially returned true to validate the test case
	 * Method has three flags, two of which check is each character is uppercase or lowercase
	 * 	changing to true if they meet the condition.
	 * The method then returns the final flag as true, only if the other two are true
	 */
	public static boolean hasUpperAndLowerChars (String pwd) {
		boolean genFlag = false, upperFlag = false, lowerFlag = false;
		
		for ( char c : pwd.toCharArray() ) {
			if ( Character.isUpperCase(c) ) {
				upperFlag = true;
			} else if (Character.isLowerCase(c) ) {
				lowerFlag = true;
			}
		}
		
		if (upperFlag && lowerFlag) {
			genFlag = true;
		}
		
		return genFlag;
	}
}
